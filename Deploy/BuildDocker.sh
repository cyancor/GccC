#!/bin/sh

if [ -z "$DockerTag" ]
then
    export DockerTag=$(cat Meta/DockerTag)
fi

if [ -z "$DockerTagLatest" ]
then
    export DockerTagLatest=$(cat Meta/DockerTagLatest)
fi

cd "$DockerFileLocation"

docker login -u $DockerHubUsername -p $DockerHubPassword

echo "docker build $DockerBuildArgs -t $DockerHubUsername/$DockerName ."
docker build \
    $DockerBuildArgs \
    -t $DockerHubUsername/$DockerName .


docker tag "$DockerHubUsername/$DockerName" "$DockerHubUsername/$DockerName:$DockerTag"
docker push "$DockerHubUsername/$DockerName:$DockerTag"

docker tag "$DockerHubUsername/$DockerName" "$DockerName:$DockerTagLatest"
docker push "$DockerHubUsername/$DockerName:$DockerTagLatest"

cd ..