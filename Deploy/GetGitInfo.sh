#!/bin/bash

echo "Gathering information..."

BuildTime=$(date --utc --iso-8601=seconds)
BuildTime=$(echo ${BuildTime/+0000/Z} )
BuildTime=$(echo ${BuildTime/+00:00/Z} )
Branch=$CI_BUILD_REF_NAME
Tag=$(git tag --contains $CI_COMMIT_SHA)
LastTag=$(git tag -l '[0-9]*\.[0-9]*\.[0-9]*' | xargs -I@ git log --format=format:"%ai @%n" -1 @ | sort | awk '{print $4}' | tail -1)
Hash=$CI_COMMIT_SHA
BuildNumber=$CI_PIPELINE_ID

if [ -z "$Branch" ]
then
    Branch="debug-build/$(git branch | grep \* | cut -d ' ' -f2)"
fi

BranchNoSlash=$(echo "$Branch" | tr / -)
BranchNoSlashLower=$(echo "$BranchNoSlash" | tr '[:upper:]' '[:lower:]')

if [ -z "$BuildNumber" ]
then
    BuildNumber=$(<Meta/BuildNumber)
    if [ -z "$BuildNumber" ]
    then
        BuildNumber=0
    else
        BuildNumber=$(($BuildNumber+1))
    fi
fi

if [ -z "$Hash" ]
then
    Hash="$(git rev-parse HEAD)"
fi

if [[ $Branch == "master" && $Tag == "" ]]; then
    echo "Detected master branch."
    echo "Tried to build the master branch without a tag."
    exit 1
fi

if [ -z "$Tag" ]
then
    Tag="0.0.0"
fi

if [ -z "$LastTag" ]
then
    LastTag="0.0.0"
fi

if [[ "$Branch" == "master" && "$Tag" =~ ^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$ ]]; then
    echo "Detected master branch."
    Category="production"
    CategoryUpper="Production"
    DockerTag="$Tag"
    DockerTagLatest="latest"
    Version="$Tag"
    VersionNumeric="$Tag"
fi

if [[ "$Branch" == "release/"* ]]; then
    echo "Detected release branch."
    Category="rc"
    CategoryUpper="Rc"
    IFS='/' read -ra Version <<< "$Branch"
    Version="${Version[1]}-$BuildNumber.rc"
    VersionNumeric="${Version[1]}"

    DockerTag="rc-$Version-$BuildNumber"
    DockerTagLatest="rc-$Version-latest"
fi

if [[ "$Branch" == "develop" ]]; then
    echo "Detected develop branch."
    Category="develop"
    CategoryUpper="Develop"
    DockerTag="develop-$BuildNumber"
    DockerTagLatest="develop-latest"
    Version="$LastTag-$BuildNumber.develop"
    VersionNumeric="$LastTag"
fi

if [[ "$Branch" == "debug-build"* ]]; then
    echo "Build from developer machine."
    Category="debug"
    CategoryUpper="Debug"
    DockerTag="debug-$BuildNumber"
    DockerTagLatest="debug-latest"
    Version="$LastTag-$BuildNumber.debug"
    VersionNumeric="$LastTag"
fi

if [[ "$Category" == "" ]]; then
    echo "No specific branch found, selecting feature."
    Category="feature"
    CategoryUpper="Feature"
    BranchCleaned=$(echo "$Branch" | sed -r 's/[\/]+/-/g')
    DockerTag="$BranchCleaned-$BuildNumber"
    DockerTagLatest="$BranchCleaned-latest"
    Version="$LastTag-$BuildNumber.$BranchNoSlashLower"
    VersionNumeric="$LastTag"
fi

mkdir -p Meta

echo "$BuildTime" > "Meta/BuildTime"
echo "$Category" > "Meta/Category"
echo "$CategoryUpper" > "Meta/CategoryUpper"
echo "$Version" > "Meta/Version"
echo "$VersionNumeric" > "Meta/VersionNumeric"
echo "$Branch" > "Meta/Branch"
echo "$BranchNoSlash" > "Meta/BranchNoSlash"
echo "$Tag" > "Meta/Tag"
echo "$Hash" > "Meta/Hash"
echo "$BuildNumber" > "Meta/BuildNumber"
echo "$DockerTag" > "Meta/DockerTag"
echo "$DockerTagLatest" > "Meta/DockerTagLatest"

echo "BuildTime:              $BuildTime"
echo "Branch:                 $Branch"
echo "BranchNoSlash:          $BranchNoSlash"
echo "Tag:                    $Tag"
echo "Hash:                   $Hash"
echo "BuildNumber:            $BuildNumber"
echo "Version:                $Version"
echo "VersionNumeric:         $VersionNumeric"
echo "Category:               $Category"
echo "CategoryUpper:          $CategoryUpper"
echo "DockerTag:              $DockerTag"
echo "DockerTagLatest:        $DockerTagLatest"
