#!/bin/sh

if [ -z "$Target" ]
then
    export Target=$CI_JOB_NAME
fi
if [ -z "$DockerBaseTag" ]
then
    export DockerBaseTag=$(cat Meta/DockerTag)
fi
if [ -z "$DockerName" ]
then
    export DockerName="$ProjectName-$Target"
fi

export DockerFileLocation="Environment"
export DockerBuildArgs="--build-arg TARGET=$Target --build-arg BASE_TAG=$DockerBaseTag --build-arg BINUTILS_VERSION=$BinutilsVersion --build-arg GCC_VERSION=$GccVersion --build-arg MAKE_MULTITHREADED="

chmod 0777 Deploy/BuildDocker.sh
./Deploy/BuildDocker.sh